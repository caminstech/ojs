# Guia ràpida d'instal·lació de l'OJS

## Requisits 

  * PHP 4.2 o posterior 
  * MySQL 3.23 o superior

## Instal·lar OJS

  1. Descarregar el software OJS des de [http://pkp.sfu.ca/ojs](http://pkp.sfu.ca/ojs/ojs_download/).

  2. Extreure els arxius al directori del servidor accessible des del navegador.

  3. Seguir les instruccions de l'arxiu README (dins del directori OJS). En cas d'error, consultar la informació a [http://pkp.sfu.ca/ojs/README](http://pkp.sfu.ca/ojs/README)

## Instal·lar els plugins

### Forma recomanada

Comprimir la carpeta que conté el plugin, i instal·lar-lo amb l'assistent d'OJS que un usuari administrador o gestor de revista pot trobar a: *User home -> System Plugins -> install a new plugin*
	
### Forma no recomanada

Copiar la carpeta que conté els arxius del plugin dins d'un dels subdirectoris de la carpeta plugins d'OJS (en el nostre cas "generic").

Per saber quin és el subdirectori, podem mirar l'arxiu version.xml del plugin, al camp "<type>" on hi ha d'haver un valor del tipus: "plugins.generic" o "plugins.nom_del_subdirectori".

Després d'haver copiat els arxius, anem a l'arrel d'OJS i d'es del terminal executem:

```
sudo php tools/upgrade.php upgrade
```