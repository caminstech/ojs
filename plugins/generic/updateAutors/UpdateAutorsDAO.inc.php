<?php

class UpdateAutorsDAO extends DAO {

	function titleExistsInDataBase($title) {
		//retorna cert si $titol existeix a la base de dades, i fals si no
		$result =& $this->retrieve(
			"SELECT setting_value FROM article_settings WHERE setting_name = 'title' AND LOWER(REPLACE(setting_value,' ','')) LIKE LOWER(REPLACE(?,' ','')) ", $title
		);

		if ($result->RecordCount() > 0) {
			return true;
		}else{
			return false;
		}
	}
	
	function getArticleIdByTitle($title){
		//retorna l'id de l'article si $title existeix a la base de dades, i -1 si no.
		$result =& $this->retrieve(
			'SELECT article_id FROM article_settings WHERE setting_name = title AND setting_value = ? LIMIT 1', $title
		);

		if ($result->RecordCount() > 0) {
			$row = $result->GetRowAssoc(false);
			return $row['article_id'];
		}else{
			return -1;
		}
	}
	
	function articlesToArray(){
		$result =& $this->retrieve(
			"SELECT a.article_id as article_id, s.setting_value as title FROM ojsgeoacta.articles a JOIN ojsgeoacta.article_settings s ON a.article_id = s.article_id WHERE s.setting_name = 'title'"
		);

		$returner = null;
		if ($result->RecordCount() != 0) {
			echo "<br>result > 0<br>";
			$returner =& $this->_rowsToArray($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function &_rowsToArray($result) {
		$i = 0;
		$artciles = array();
		
		while (!$result->EOF) {
			$row = $result->GetRowAssoc(false);
			$artciles[$i]['article_id'] = $row['article_id'];
			$artciles[$i]['title'] = $row['title'];
			$result->MoveNext();
			$i++;
		}
		echo "<br>i = $i<br>";
		return $artciles;
	}
	
	function updateArticle($id,$data) {
		//actualitzem date_submitted de l'article
		if($data != "") {
			$this->update(
				'UPDATE articles SET date_submitted = (STR_TO_DATE(?,"%d/%m/%Y")) WHERE article_id = ?',
				array(
					$data,
					$id
				)
			);
		}
	}
	
	function updateArticleSettings($id,$direccio) {
		//afegim un setting amb nom "address", que es la direccio des d'on s'ha escrit l'article
		$this->update(
			"INSERT INTO article_settings
				(article_id,setting_name,setting_value,locale,setting_type)
				VALUES
				(?, 'address', ?,'en_US','string')",
			array(
				$id,
				$direccio
			)
		);
	}
	
	function insertNouArticle($titol,$autor,$data,$direccio) {
		$titol = addslashes($titol);
		$autor = addslashes($autor);
		$direccio = addslashes($direccio);
		
		//primer, inserim l'article
		$this->update(
			"INSERT INTO articles
				(locale,user_id,journal_id,section_id,date_submitted,last_modified,date_status_modified,status,submission_progress,current_round,fast_tracked,hide_author,comments_status)
				VALUES
				('en_US',1,1,1,?,?,?,4,0,1,0,0,0)",
			array(
				$data,
				$data,
				$data
			)
		);
		
		//i consultem l'id de l'article que acabem d'inserir
		$result =& $this->retrieve(
				'SELECT LAST_INSERT_ID() as article_id'
			);
			
		echo "insert article<br>";
		if ($result->RecordCount() > 0) {
			$row = $result->GetRowAssoc(false);
			$idArticle =  $row['article_id'];
		}else{
			return -1;
		}

		//insertem l'autor
		$this->update(
				"INSERT INTO authors
					(submission_id,first_name)
					VALUES
					(?,?)",
				array(
					$idArticle,
					$autor
				)
			);
		
		//insertem la direcció i el títol a article_settings
		$this->update(
			"INSERT INTO article_settings
				(article_id,setting_name,setting_value,locale,setting_type)
				VALUES
				(?, 'adress', ?,'en_US','string')",
			array(
				$idArticle,
				$direccio
			)
		);
		
		//insertem el titol
		$this->update(
			"INSERT INTO article_settings
				(article_id,setting_name,setting_value,locale,setting_type)
				VALUES
				(?, 'title', ?,'en_US','string')",
			array(
				$idArticle,
				$titol
			)
		);
		
		//insertem el clean title, un títol que només té espais i caracters alfanumèrics sense símbols 
		$cleanTitle = preg_replace('/[^a-zA-Z0-9_ ]/s', '', $titol);
		
		$this->update(
			"INSERT INTO article_settings
				(article_id,setting_name,setting_value,locale,setting_type)
				VALUES
				(?, 'cleanTitle', ?,'en_US','string')",
			array(
				$idArticle,
				$cleanTitle
			)
		);
	}
}

?>