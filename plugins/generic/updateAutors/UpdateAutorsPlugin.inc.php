<?php
import('classes.plugins.GenericPlugin');
class UpdateAutorsPlugin extends GenericPlugin {
    function register($category, $path) {
        if (parent::register($category, $path)) {
            HookRegistry::register('Templates::Manager::Index::Users', array(&$this, 'callback')); 
            return true;
        }
        return false;
    }
    function getName() {
        return 'UpdateAutorsPlugin';
    }
    function getDisplayName() {
        return 'UpdateAutors Plugin';
    }
    function getDescription() {
        return 'A description of this plugin';
    }
    
	function getCSV() {
    	echo "getCSV";
    	$arxiu = "autors.csv";
    	$sortida = "files/autors_parsejat.csv";
    	$articlesExistens = "files/articlesExistents.csv";
    	$articlesRebutjats = "files/articlesRebutjats.csv";
    	
    	//en aquest arxiu guardarem l'id i la data que insertarem/actualitzarem a l'article amb aquest id
    	$arxiuArticles = "files/articleAmbIdData.csv";
    	//en aquest arxiu guardarem les dades de la taula article settings per a l'article amb aquest id
    	$arxiuArticleSettings = "files/articleSettingsAmbDireccioTitolIdArticle.csv";
    	//en aquest guardarem la info dels que no tenen un titol similar a la bbdd
    	$arxiuNousArticles = "files/nousArticlesPerInserir.csv";
    	
    	//carreguem la capa de dades
        $this->import('UpdateAutorsDAO');
		$updateAutorsDAO = new UpdateAutorsDAO();
		DAORegistry::registerDAO('UpdateAutorsDAO', $updateAutorsDAO);
		
    	$articlesBD = $updateAutorsDAO->articlesToArray();
    	echo "count articlesBD: ".count($articlesBD)."<br>";
		
		$contTotal = 0;
		$contExistents = 0;
		$contNous = 0;
		
    	if (($handle = fopen($arxiu, "r")) != FALSE) {
    		
    		//obrim els 3 arxius on guardarem la info
    		$articlesHandle = fopen($arxiuArticles,"w");
    		$articleSettingsHandle = fopen($arxiuArticleSettings,"w");
    		$nousArticlesHandle = fopen($arxiuNousArticles,"w");
    		
    		//recorrem tot l'arxiu autors.csv linia a linia
			while (($data = fgetcsv($handle, 0, "	")) != FALSE) {
		        $num = count($data);
		        
		        $autors = $data[0];
		        $autors = str_replace("(1)","",$autors);
		        $autors = str_replace("(2)","",$autors);
		        $autors = str_replace("(3)","",$autors);
		        $autors = str_replace("(4)","",$autors);
		        $autors = str_replace("(5)","",$autors);
		        $autors = str_replace("(6)","",$autors);
		        $autors = str_replace("(7)","",$autors);
		        $autors = str_replace("(8)","",$autors);
		        $autors = str_replace("(|)","",$autors);
		        $autors = str_replace("( et al.)","",$autors);
		        $autors = str_replace("(')","",$autors);
		        $autors = str_replace("--salt-de-linia--", " ", $autors);
		        $autorsSeparats = preg_split(" [\sand\s|(,\s)|\sy\s|(\x26\s)] ",$autors);
		        
		        $direccio = $data[2];
		        $direccio = str_replace("--salt-de-linia--", " ", $direccio);
		        
		        $titolParsejat = strtolower(str_replace("--salt-de-linia--", " ", $data[3]));
		        $titolParsejat = str_replace(";","", $titolParsejat);
		        $titolParsejat = str_replace(",","", $titolParsejat);
		        
		        //Aqui per cada titol, comparem si s'assembla a aquest (via l'algoritme de levenstein), 
		        //i en cas de que s'hi assembli, guardem l'id de l'article al que s'assembla
				$maxSimilitud = 0;
				$titolSimilarTrobat = false;
				$posTitolMesSimilar = 0;
				
		        for($e = 0; $e < count($articlesBD); $e++) {
		        	
		        	$titolALaBD = $articlesBD[$e]['title'];
		        	$titolALaBD = str_replace(",", "", $titolALaBD);
					$tempSimilitud = $this->similitud(strtolower($titolParsejat), strtolower($titolALaBD));
					if($maxSimilitud < $tempSimilitud) {
						$maxSimilitud = $tempSimilitud;
						$posTitolMesSimilar = $e;
					}
					
			        if($tempSimilitud >= 70) {
			        	//Actualitzem un article existent
						$contExistents++;
						$titolSimilarTrobat = true;
			        }
				}
				
				//depenent de si l'hem trobat o no, el guardem a un o altre arxiu
				if($titolSimilarTrobat){
					$idArticleMesSimilar = $articlesBD[$posTitolMesSimilar]['article_id'];
					$fecha = $data[1];
					/*escribim aquests camps de l'article perquè més endavant actualitzarem les taules
					articles (amb la data) i article_settings (inserirem la nova direccio)*/
					fwrite($articlesHandle,"$idArticleMesSimilar;$fecha;\n");
					fwrite($articleSettingsHandle,"$idArticleMesSimilar;$direccio;\n");

				}else{
				    /*escribim el nou article, article settings, i autors perquè més endavant inserirem 
				    nous registres a les taules articles, authors, i article_settings*/
					fwrite($nousArticlesHandle,"$titolParsejat;$autors;$data[1];$direccio;\n");
					$contNous++;
				}
				
				$titolSimilarTrobat = false;
		        $contTotal++;
		        $row++;
		        
		    }
		    fclose($articleSettingsHandle);
		    fclose($articlesHandle);
		    fclose($nousArticlesHandle);
		    fclose($handle);
		}else{
			echo "handle false";
		}
		echo "<br>existents: $contExistents<br>nous: $contNous<br>total: $contTotal<br>";
		return true;
    }

	function similitud($string1,$string2) {
		// retorna el % de similitud entre l'string1 i l'string2 utilitzan l'algoritme de levenstein
    	$distanciaLevenshtein = levenshtein($string1,$string2);
    	
    	if($distanciaLevenstein >= 0) {
    		$similitudPerCent = ((strlen($string1))-$distanciaLevenshtein)/(strlen($string1))*100;
    	}else{
    		echo "Error de levenshtein, string massa llarg:<br>$string1<br>$string2<br>";
    		$similitudPerCent = 0;
    	}
    	
    	return $similitudPerCent;
    }
    
    function updateDB(){
    	//carreguem la capa de dades
        $this->import('UpdateAutorsDAO');
		$updateAutorsDAO = new UpdateAutorsDAO();
		DAORegistry::registerDAO('UpdateAutorsDAO', $updateAutorsDAO);
		
		//obrim els arxius on tenim la info (potser caldria moure-ho a la carpeta del plugin
    	$arxiuArticles = "files/articleAmbIdData.csv";
    	$arxiuArticleSettings = "files/articleSettingsAmbDireccioTitolIdArticle.csv";
    	$arxiuNousArticles = "files/nousArticlesPerInserir.csv";
    	
    	$articlesHandle = fopen($arxiuArticles,"r");
    	$articleSettingsHandle = fopen($arxiuArticleSettings,"r");
    	$nousArticlesHandle = fopen($arxiuNousArticles,"r");
    	
    	//actualitzem cada article amb la nova data
    	while (($data = fgetcsv($articlesHandle, 0, ";")) != FALSE) {
    		if($data[1] != ""){
    			$updateAutorsDAO->updateArticle($data[0], $data[1]);
    		}
    	}
    	
    	//creem un nou setting pel valor "direccio" per a cada article
    	while (($data = fgetcsv($articleSettingsHandle, 0, ";")) != FALSE) {
    		$updateAutorsDAO->updateArticleSettings($data[0], $data[1]);
    	}
    	
    	//inserim tots els articles que no hi fossin (creem un nou autor, article, i nous settings
    	while (($data = fgetcsv($nousArticlesHandle, 0, ";")) != FALSE) {
    		$updateAutorsDAO->insertNouArticle($data[0],$data[1],$data[2],$data[3]);
    	}
    	
    	 fclose($articleSettingsHandle);
		 fclose($articlesHandle);
		 fclose($nousArticlesHandle);
    }
    
    function callback($hookName, $args) {
        $params =& $args[0];
        $smarty =& $args[1];
        $output =& $args[2];
        
        $templateMgr =& TemplateManager::getManager();
        $templateMgr.flush();
        //si ha clicat update:
		if(isset($_POST['update'])){
			if($_POST['update'] == 1){
				$this->updateDB();
        		$templateMgr->assign('updated', 1);
			}else{
				$templateMgr->assign('updated', 0);
			}
		}else{
			$templateMgr->assign('updated', 0);
		}
		
        $path = $this->getTemplatePath();
        
        $templateMgr->display($path . 'index.tpl');
        return false;
    }
    
	function getManagementVerbs() {
		$verbs = array();
		if ($this->getEnabled()) {
			$verbs[] = array('disable',Locale::translate('manager.plugins.disable'));
			$verbs[] = array('settings',Locale::translate('manager.plugins.settings'));
		} else {
			$verbs[] = array('enable',Locale::translate('manager.plugins.enable'));
		}
		return $verbs;
	}
}
