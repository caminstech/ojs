<?php
import('classes.plugins.GenericPlugin');

class BackPlugin extends GenericPlugin {
    function register($category, $path) {
        if (parent::register($category, $path)) {
            HookRegistry::register('Templates::Article::Footer::PageFooter', array(&$this, 'callback'));
        	return true;
        }
        return false;
    }
    function getName() {
        return 'BackPlugin';
    }
    function getDisplayName() {
        return 'Back Plugin';
    }
    function getDescription() {
        return 'Click en el texto "atras" para retorceder un paso en el historial web. Igual que con la funcion atras del navegador.';
    }
    function callback($hookName, $args) {
        $params =& $args[0];
        $smarty =& $args[1];
        $output =& $args[2];
        
        //Busquem l'id de l'issue perquè le template pugui crear el link a l'issue (back)
        $num_issue = ($args[1]->_tpl_vars[issue]->_data[id]);
		$templateMgr =& TemplateManager::getManager();
		$templateMgr->assign('num_issue', $num_issue);
        $path = $this->getTemplatePath();
        
        //crec que cal treure aquest path
        //$templateMgr->display($path . 'index.tpl');
        $templateMgr->display($path . 'index.tpl');

        return false;
    }
    
	function getManagementVerbs() {
		$verbs = array();
		if ($this->getEnabled()) {
			$verbs[] = array('disable',Locale::translate('manager.plugins.disable'));
			//$verbs[] = array('settings',Locale::translate('plugins.generic.ejme.settings'));
		} else {
			$verbs[] = array('enable',Locale::translate('manager.plugins.enable'));
		}
		return $verbs;
	}
}
