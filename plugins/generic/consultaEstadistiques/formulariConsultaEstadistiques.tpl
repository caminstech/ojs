{**
 * formulariConsultaEstadistiques.tpl
 *
 * 
 *
 * 
 *}
 {strip}
{assign var="pageTitle" value="plugins.generic.consultaEstadistiques"}
{include file="common/header.tpl"}
{/strip}
<head>
<link rel="stylesheet" href="{$baseUrl}/plugins/generic/consultaEstadistiques/css/estils.css" type="text/css" />
<link rel="stylesheet" href="{$baseUrl}/plugins/generic/consultaEstadistiques/js/date_picker/jsDatePick_ltr.css" type="text/css" />
<script type="text/javascript" src="{$baseUrl}/jsDatePick.full.1.3.js"></script>
</head>
<!-- <script type="tex	t/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->

<div id="formDiv">
{translate key="plugins.generic.consultaEstadistiques.instruccions"}
<br><br><br>
<form method="post" action="{url page=ConsultaEstadistiques op=resultatConsulta}" id="formConsultarEstadistiques">
<table border=0px style="vertical-align: top;" border-spacing="5">
	<tr class="trEst">
		<td rowspan="3">Autors:</td>
		</tr>
		<tr>
			<td><input type=text name="buscadorAutor" id = "buscadorAutor" class="textBuscador" autocomplete=off></td>
		</tr>
		<tr>
		<td>
			<select multiple name="autors[]" id="autors" class="multiselectAjax">
			{foreach item=autor from=$autors}
				<option value={$autor.user_id}>{$autor.username}</option>
			{/foreach}
			</select>
		<td></td>
		
		<td class="tdBtnEliminar"><button id="esborrarTotsAutors" class="buttonEsborrarSeleccionats">Eliminar</button></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr class="trEst">
		<td rowspan="3">Editors:</td>
		</tr>
		<tr>
			<td><input type=text name="buscadorEditor" id = "buscadorEditor" class="textBuscador" autocomplete=off></td>
		</tr>
		<tr>
		<td>
			<select multiple name="editors[]" id="editors" class="multiselectAjax">
			{foreach item=editor from=$editors}
				<option value={$editor.user_id}>{$editor.username}</option>
			{/foreach}
			</select>
		<td></td>
		
		<td class="tdBtnEliminar"><button id="esborrarTotsEditors" class="buttonEsborrarSeleccionats">Eliminar</button></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr class="trEst">
		<td rowspan="3">Paisos dels autors:</td>
	</tr>
	<tr>
		<td><input type=text name="buscadorPaisAutor" id = "buscadorPaisAutor" class="textBuscador" autocomplete=off></td>
	</tr>
	<tr>
		<td>
			<select multiple name="paisAutors[]" id="paisAutors" class="multiselectAjax">
			{foreach item=paisAutor from=$paisosAutors}
				<option value={$paisAutor.key}>{$paisAutor.name}</option>
			{/foreach}
			</select>
		<td></td>
		<td class="tdBtnEliminar"><button id="esborrarTotsPaisAutor" class="buttonEsborrarSeleccionats">Eliminar</button></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr class="trEst">
		<td rowspan="3">Paisos dels articles:</td>
	</tr>
	<tr>
		<td><input type=text name="buscadorPaisArticle" id = "buscadorPaisArticle" class="textBuscador" autocomplete=off></td>
	</tr>
	<tr>
		<td>
		<select multiple name="paisArticles[]" id="paisArticles" class="multiselectAjax">
			{foreach item=paisArticle from=$paisosArticles}
				<option value={$paisArticle.key}>{$paisArticle.name}</option>
			{/foreach}
			</select>
		<td></td>
		<td class="tdBtnEliminar"><button id="esborrarTotsPaisArticle" class="buttonEsborrarSeleccionats">Eliminar</button></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr class="trEst">
		<td rowspan="3">Revisors:</td>
	</tr>
	<tr>
		<td><input type=text name="buscadorRevisor" id = "buscadorRevisor" class="textBuscador" autocomplete=off></td>
	</tr>
	<tr>
		<td>
			<select multiple name="revisors[]" id="revisors" class="multiselectAjax">
			{foreach item=revisor from=$revisors}
				<option value={$autor.user_id}>{$revisor.username}</option>
			{/foreach}
			</select>
		<td></td>
		<td class="tdBtnEliminar"><button id="esborrarTotsRevisors" class="buttonEsborrarSeleccionats">Eliminar</button></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr class="trEst">
		<td rowspan="3">Keywords dels articles:</td>
	</tr>
	<tr>
		<td><input type=text name="buscadorKeyword" id = "buscadorKeyword" class="textBuscador" autocomplete=off></td>
	</tr>
	<tr>
		<td>
			<select multiple name="keywords[]" id="keywords" class="multiselectAjax">
			{foreach item=paisAutor from=$paisosAutors}
				<option value={$keywords.key}>{$keywords.name}</option>
			{/foreach}
			</select>
		<td></td>
		<td class="tdBtnEliminar"><button id="esborrarTotsKeywords" class="buttonEsborrarSeleccionats">Eliminar</button></td>
	</tr>
	<tr>
	<tr class="trEst">
		<td>Data inicial d'enviament: </td>
		<td><input type="text" name="dataIniciSubmitted" id="dataIniciSubmitted" class="textData" placeholder="dd/mm/aaaa"></td>
	</tr>
	<tr class="trEst">
		<td>Data final d'enviament: </td>
		<td><input type="text" name="dataFiSubmitted" id="dataFiSubmitted" class="textData" placeholder="dd/mm/aaaa"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr class="trEst">
		<td>Data inicial d'assignació: </td>
		<td><input type="text" name="dataIniciAssigned" id="dataIniciAssigned" class="textData" placeholder="dd/mm/aaaa"></td>
	</tr>
	<tr class="trEst">
		<td>Data final d'assignació: </td>
		<td><input type="text" name="dataFiAssigned" id="dataFiAssigned" class="textData" placeholder="dd/mm/aaaa"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr class="trEst">
		<td>Data inicial d'entrega de la versió revisada: </td>
		<td><input type="text" name="dataIniciCompleted" id="dataIniciCompleted" class="textData" placeholder="dd/mm/aaaa"></td>
	</tr>
	<tr class="trEst">
		<td>Data final d'entrega de la versió revisada: </td>
		<td><input type="text" name="dataFiCompleted" id="dataFiCompleted" class="textData" placeholder="dd/mm/aaaa"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr class="trEst">
		<td></td>
		<td><input type="submit" value="Fer consulta" class="btnSubmit"></td>
	</tr>
</table>
</form>
</div>
<script language="javascript">
{literal}
//aquest template en cas de tenir javascript desactivat funcionaria igual (falta afegir algun camp)
//en cas de tenir javascripot desactivat borra tot el que hi ha als selects i ho mostra e
$(document).ready(function(){

	var valorsAutors =[];
	var valorsAutorsLV = [];
	var valorsRevisors =[];
	var provaJSON;
	var valorsPaisAutor =[];
	var valorsPaisArticle=[];
	var valorsKeywords=[];
	var valorsEditors=[];

	//esborrat
	$("#esborrarTotsAutors").click(function(){
		$("#autors option:selected").remove();
		return false;
	});
	$("#esborrarTotsEditors").click(function(){
		$("#editors option:selected").remove();
		return false;
	});
	$("#esborrarTotsPaisAutor").click(function(){
		$("#paisAutors option:selected").remove();
		return false;
	});
	$("#esborrarTotsPaisArticle").click(function(){
		$("#paisArticles option:selected").remove();
		return false;
	});
	$("#esborrarTotsRevisors").click(function(){
		$("#revisors option:selected").remove();
		return false;
	});
	$("#esborrarTotsKeywords").click(function(){
		$("#keywords option:selected").remove();
		return false;
	});
	
	$("#buttonAfegirAutor").click(function(){
		if ($("#buscadorAutor").val() != ""){
			if(tagsConte(valorsAutors,$("#buscadorAutor").val())){
				if($("#textAreaAutor").val() == ""){
					$("#textAreaAutor").val($("#buscadorAutor").val());
				}else{
					$("#textAreaAutor").val($("#textAreaAutor").val()+'\n'+ ($("#buscadorAutor").val()) );
				}
				//aqui afegim al multiselect
				$("#autors").append($("<option></option>").attr("value", $("#idAutors").val()).text($("#buscadorAutor").val()));
				$("#buttonAfegirAutor").attr("disabled", "disabled");
			}
		}
		return false;
	});
	$("#buttonAfegirPaisAutor").click(function(){
		if ($("#buscadorPaisAutor").val() != ""){
			if(tagsConte(valorsPaisAutors,$("#buscadorPaisAutor").val())){
				if($("#textAreaPaisAutor").val() == ""){
					$("#textAreaPaisAutor").val($("#buscadorPaisAutor").val());
				}else{
					$("#textAreaPaisAutor").val($("#textAreaPaisAutor").val()+'\n'+ ($("#buscadorPaisAutor").val()) );
				}
				$("#paisAutors").append($("<option></option>").attr("value", $("#idPaisAutor").val()).text($("#buscadorPaisAutor").val()));
				$("#buttonAfegirAutor").attr("disabled", "disabled");
			}
		}
		return false;
	});
	$("#buttonAfegirPaisArticle").click(function(){
		if ($("#buscadorPaisArticle").val() != ""){
			if(tagsConte(valorsPaisArticle,$("#buscadorPaisArticle").val())){
				if($("#textAreaPaisArticle").val() == ""){
					$("#textAreaPaisArticle").val($("#buscadorPaisArticle").val());
				}else{
					$("#textAreaPaisArticle").val($("#textAreaPaisArticle").val()+'\n'+ ($("#buscadorPaisArticle").val()) );
				}
			}
		}
		return false;
	});
	$("#buttonAfegirRevisor").click(function(){
		if ($("#buscadorRevisor").val() != ""){
			if(tagsConte(valorsRevisors,$("#buscadorRevisor").val())){
				if($("#textAreaRevisor").val() == ""){
					$("#textAreaRevisor").val($("#buscadorRevisor").val());
				}else{
					$("#textAreaRevisor").val($("#textAreaRevisor").val()+'\n'+ ($("#buscadorRevisor").val()) );
				}
				$("#revisors").append($("<option></option>").attr("value", $("#idRevisors").val()).text($("#buscadorRevisor").val()));
				$("#buttonAfegirRevisor").attr("disabled", "disabled");
			}
		}
		return false;
	});
	
	var param = "";
	$.ajax({
		data:  param,
		url:   '{/literal}{url page=ConsultaEstadistiques op=ajaxGetAutorsJSON}{literal}',
		type:  'post',
		beforeSend: function () {
			//$("#result").html("Processing...");
		 },
		success:  function (response) {

			provaJSON = JSON.parse(response);
			
			for (var i = 0; i < provaJSON.length; i++) {
				valorsAutors.push({"value": provaJSON[i].user_id,"label": provaJSON[i].username});
			}			
			$("#buscadorAutor").autocomplete({
				source: valorsAutors,
				minLength: 2,
				select: function(event, ui){
					$("#autors").append($("<option></option>").attr("value", ui.item.value).text(ui.item.label));
					$("#buscadorAutor").val("");
					return false;
				}
			});
		}
	});
	
	$.ajax({
		data:  param,
		url:   '{/literal}{url page=ConsultaEstadistiques op=ajaxGetEditorsJSON}{literal}',
		type:  'post',
		success:  function (response) {
			provaJSON = JSON.parse(response);
			
			for (i = 0; i < provaJSON.length; i++) {
				valorsEditors.push({"value": provaJSON[i].user_id,"label": provaJSON[i].username});
			}
			$( "#buscadorEditor" ).autocomplete({
				source: valorsEditors,
				minLength: 2,
				select: function(event, ui){
					$("#editors").append($("<option></option>").attr("value", ui.item.value).text(ui.item.label));
					$("#buscadorEditor").val("");
					return false;
				}
			});
		}
	});
	
	$.ajax({
		data:  param,
		url:   '{/literal}{url page=ConsultaEstadistiques op=ajaxGetRevisorsJSON}{literal}',
		type:  'post',
		success:  function (response) {
			provaJSON = JSON.parse(response);
			
			for (i = 0; i < provaJSON.length; i++) {
				valorsRevisors.push({"value": provaJSON[i].user_id,"label": provaJSON[i].username});
			}
			$( "#buscadorRevisor" ).autocomplete({
				source: valorsRevisors,
				minLength: 2,
				select: function(event, ui){
					$("#revisors").append($("<option></option>").attr("value", ui.item.value).text(ui.item.label));
					$("#buscadorRevisor").val("");
					return false;
				}
			});
		}
	});
	
	$.ajax({
		data:  param,
		url:   '{/literal}{url page=ConsultaEstadistiques op=ajaxGetPaisosAutorsJSON}{literal}',
		type:  'post',
		success:  function (response) {
			provaJSON = JSON.parse(response);
			
			for (i = 0; i < provaJSON.length; i++) {
				valorsPaisAutor.push({"value": provaJSON[i].key,"label": provaJSON[i].name});
			}
			$( "#buscadorPaisAutor" ).autocomplete({
				source: valorsPaisAutor,
				
				select: function(event, ui){
				$("#paisAutors").append($("<option></option>").attr("value", ui.item.value).text(ui.item.label));
				$("#buscadorPaisAutor").val("");
				return false;
			}
			});
		}
	});
	$.ajax({
		data:  param,
		url:   '{/literal}{url page=ConsultaEstadistiques op=ajaxGetPaisosArticlesJSON}{literal}',
		type:  'post',
		success:  function (response) {
			provaJSON = JSON.parse(response);
			
			for (i = 0; i < provaJSON.length; i++) {
				valorsPaisArticle.push({"value": provaJSON[i].key,"label": provaJSON[i].name});
			}
			$( "#buscadorPaisArticle" ).autocomplete({
				source: valorsPaisArticle,
				//minLength: 2,
				select: function(event, ui){
				$("#paisArticles").append($("<option></option>").attr("value", ui.item.value).text(ui.item.label));
				$("#buscadorPaisArticle").val("");
				return false;
			}
			});
		}
	});
	$.ajax({
		data:  param,
		url:   '{/literal}{url page=ConsultaEstadistiques op=ajaxGetKeywordsJSON}{literal}',
		type:  'post',
		success:  function (response) {
			provaJSON = JSON.parse(response);
			
			for (i = 0; i < provaJSON.length; i++) {
				valorsKeywords.push({"value": provaJSON[i].keyword_text,"label": provaJSON[i].keyword_text});
			}
			$( "#buscadorKeyword" ).autocomplete({
				source: valorsKeywords,
				minLength: 3,
				select: function(event, ui){
					$("#keywords").append($("<option></option>").attr("value", ui.item.value).text(ui.item.label));
					$("#buscadorKeyword").val("");
					return false;
				}
			});
		}
	});
});
function tagsConte(arr,element){
	//verifiquem que a arr hi hagi un element amb label = l'element
	var i;
	for(i=0;i<arr.length;i++){
		if(arr[i].label == element){
			return true;
		}
	}
	return false;
}
$(function() {
	//aquesta funció posa els calendaris als camps de data
	$( "#dataIniciSubmitted" ).datepicker();
	$( "#dataIniciSubmitted" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
	$( "#dataFiSubmitted" ).datepicker();
	$( "#dataFiSubmitted" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
	$( "#dataIniciAssigned" ).datepicker();
	$( "#dataIniciAssigned" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
	$( "#dataFiAssigned" ).datepicker();
	$( "#dataFiAssigned" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
	$( "#dataIniciCompleted" ).datepicker();
	$( "#dataIniciCompleted" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
	$( "#dataFiCompleted" ).datepicker();
	$( "#dataFiCompleted" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
});
$( "#formConsultarEstadistiques" ).submit(function( event ) {
	$("#autors > option").each(function(){
	    $(this).attr("selected",true);
	});
	$("#editors > option").each(function(){
	    $(this).attr("selected",true);
	});
	$("#paisAutors > option").each(function(){
	    $(this).attr("selected",true);
	});
	$("#paisArticles > option").each(function(){
	    $(this).attr("selected",true);
	});
	$("#revisors > option").each(function(){
	    $(this).attr("selected",true);
	});
	$("#keywords > option").each(function(){
	    $(this).attr("selected",true);
	});
});
{/literal}
</script>