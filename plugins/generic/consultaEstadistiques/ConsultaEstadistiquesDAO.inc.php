<?php

class ConsultaEstadistiquesDAO extends DAO {
	
	var $fromWhere;
	var $countries;
	
	function ConsultaEstadistiquesDAO($journalId){
		parent::__construct();
		
		$countryDao =& DAORegistry::getDAO('CountryDAO');
		$this->countries =& $countryDao->getCountries();
	}
	
	/*function contRebutjatsAutor($nomAutor,$dataInici,$dataFi){
		//retorna el número d'articles rebutjats d'un autor
		if( ($this ->comprovarParametre($nomAutor)) and ($this ->comprovarParametre($dataInici)) and ($this->comprovarParametre($dataFi)) ){
			//crec que status 4 era rebutjat
			$result =& $this->retrieve(
				"SELECT count(article_id) as total FROM articles WHERE status = 4 AND article_id IN
				(SELECT submission_id FROM authors WHERE first_name like '%$nomAutor%' OR last_name like '%$nomAutor%' OR middle_name like '%$nomAutor%' ) "/*,$dataInici,$dataFi* /
			);
			
			$row = $result->GetRowAssoc(false);
			return $row['total'];
		}else{
			return null;
		}
	}
	
	function contPublicatsAutor($nomAutor,$dataInici,$dataFi){
		//retorna el número d'articles publicats d'un autor
		if( ($this ->comprovarParametre($nomAutor)) and ($this ->comprovarParametre($dataInici)) and ($this ->comprovarParametre($dataFi)) ){
			//status = 3 = publicat?
			$result =& $this->retrieve(
				"SELECT count(article_id) as total FROM articles WHERE status = 3 AND article_id IN
				(SELECT submission_id FROM authors WHERE first_name like '%$nomAutor%' OR last_name like '%$nomAutor%' OR middle_name like '%$nomAutor%' ) "/*,$dataInici,$dataFi*
			);
			
			$row = $result->GetRowAssoc(false);
			return $row['total'];
		}else{
			return null;
		}
	}
	
	function contPercentatgePublicacioAutor($nomAutor,$dataInici,$dataFi){
		//retorna el percentatge d'articles publicats d'un autor respecte el total de pulbicats i rebutjats
		$publicats = & $this ->contPublicatsAutor($nomAutor,$dataInici,$dataFi);
		$rebutjats = & $this ->contRebutjatsAutor($nomAutor,$dataInici,$dataFi);
		
		$total = $publicats + $rebutjats;
		
		if($total > 0){
			$percentatge = ($publicats/$total) * 100;
		}else{
			$percentatge = 0;
		}
		return $percentatge;
	}
	
	function contTotalAutor($nomAutor,$dataInici,$dataFi){
		//retorna el número d'articles publicats
		$acceptats = & $this -> contPublicatsAutor($nomAutor,$dataInici,$dataFi);
		$rebutjats = & $this -> contRebutjatsAutor($nomAutor,$dataInici,$dataFi);
		$percentatge = & $this -> contPercentatgePublicacioAutor($nomAutor,$dataInici,$dataFi);
		
		$resultat = array($acceptats,$rebutjats,$percentatge);
		return $resultat;
	}*/
	
	function comprovarParametre($param){
		if( ($param != null) and ($param != '') ){
			return true;
		}else{
			return false;
		}
	}
	
	function getAutors($journalId){
		
		//a la taula roles el rol d'autor s'identifica per un role_id = 65536... Chapuza dels d'OJS
		$result =& $this->retrieve(
			"SELECT u.user_id, u.username, u.first_name, u.last_name 
			FROM users u, roles r
			WHERE r.journal_id = ? AND u.user_id = r.user_id AND r.role_id = 65536",$journalId
		);
		
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArray($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function getAutorsAssoc($journalId){
		
		//a la taula roles el rol d'autor s'identifica per un role_id = 65536... Chapuza dels d'OJS
		$result =& $this->retrieve(
			"SELECT u.user_id, u.username, u.first_name, u.last_name 
			FROM users u, roles r
			WHERE r.journal_id = ? AND u.user_id = r.user_id AND r.role_id = 65536",$journalId
		);
		
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArrayAssoc($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function getNomAutorsByIdAssoc($arrayIdAutors){
		
		$inIdAutors = $this -> arrayToComaSeparatedString($arrayIdAutors);
		
		//a la taula roles el rol d'autor s'identifica per un role_id = 65536... Chapuza dels d'OJS
		$result =& $this->retrieve(
			"SELECT u.first_name, u.last_name 
			FROM users u 
			WHERE u.user_id IN ($inIdAutors)"
		);
		
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArrayAssoc($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function getRevisors($journalId){
		$result =& $this->retrieve(
			"SELECT u.user_id, u.username, u.first_name, u.last_name 
			FROM users u, roles r
			WHERE r.journal_id = ? AND u.user_id = r.user_id AND r.role_id = 4096",$journalId
		);
		
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArray($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function getRevisorsAssoc($journalId){
		$result =& $this->retrieve(
			"SELECT u.user_id, u.username, u.first_name, u.last_name 
			FROM users u, roles r
			WHERE r.journal_id = ? AND u.user_id = r.user_id AND r.role_id = 4096",$journalId
		);
		
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArrayAssoc($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function getEditorsAssoc($journalId){
		$result =& $this->retrieve(
			"SELECT u.user_id, u.username, u.first_name, u.last_name 
			FROM users u, roles r
			WHERE r.journal_id = ? AND u.user_id = r.user_id AND r.role_id = 256",$journalId
		);
		
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArrayAssoc($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function getPaisosAutors($journalId){
		$result =& $this->retrieve(
			"SELECT distinct(u.country) 
			FROM users u, articles a
			WHERE a.journal_id = ? AND u.user_id = a.user_id AND u.country is not null ",$journalId
		);
		
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArray($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function getPaisosArticles($journalId){
		$result =& $this->retrieve(
			"SELECT distinct(a.country) 
			FROM autors a, articles art
			WHERE a.submission_id =  art.article_id AND art.journal_id = ? ",$journalId
		);
		
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArray($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function getPaisosArticlesAssoc($journalId){
		$result =& $this->retrieve(
			"SELECT distinct(a.country) 
			FROM authors a, articles art
			WHERE a.submission_id =  art.article_id AND art.journal_id = ? ",$journalId
		);
		
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArrayAssoc($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function getPaisosAutorsAssoc($journalId){
		$result =& $this->retrieve(
			"SELECT distinct(u.country) 
			FROM users u, articles a
			WHERE a.journal_id = ? AND u.user_id = a.user_id AND u.country is not null ",$journalId
		);
		
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArrayAssoc($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function getKeywordsAssoc($journalId){
		$result =& $this->retrieve(
			"SELECT distinct(askl.keyword_text)
			FROM articles art, article_search_objects aso, article_search_object_keywords asok, article_search_keyword_list askl
			WHERE art.article_id = aso.article_id AND aso.object_id = asok.object_id AND asok.keyword_id = askl.keyword_id AND art.journal_id = ? ",$journalId
		);
		
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArrayAssoc($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function contRebutjats($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted, $journalId,$textKeywords){
		$status = 0;
		return $this -> contArticlesDUnEstat($status,$idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords);
	}
	
	function contPublicats($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords){
		$status = 3;
		return $this -> contArticlesDUnEstat($status,$idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords);
	}
	
	function contArticlesDeQualsevolEstat($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords){
		$status = null;
		return $this -> contArticlesDUnEstat($status,$idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords);
	}
	
	private function setFromWhere($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$status,$keywords,$additionalFrom = null,$additionalWhere = null){
		$where = "";
		
		if(! is_null($status) ){
			$where .= " AND a.status = $status ";
		}
		
		if(! is_null($idAutors)){
			$inAutors = $this -> arrayToComaSeparatedString($idAutors);
			$where .= " AND a.user_id IN ($inAutors) ";
		}
		
		if ( ! is_null($idEditors) ){
			//aquí caldria modificar el from per afegir la join.
			//Només cal afegir la join si escollim el que sigui de l'altra taula
			$additionalFrom.= " , edit_assignments ea ";
			$inEditors = $this -> arrayToComaSeparatedString($idEditors);
			$where .= " AND a.article_id = ea.article_id AND ea.editor_id IN ($inEditors) ";
		}
		if(! is_null($paisosAutors)){
			$inPaisosAutors = $this -> arrayToComaSeparatedQuotedString($paisosAutors);
			$where .= " AND u.country IN ($inPaisosAutors) ";
			//modo subquery AND user_id IN (SELECT user_id FROM users WHERE country IN ($inPaisosAutors) ) ";
		}
		if(! is_null($paisosArticles)){
			$inPaisosArticles = $this -> arrayToComaSeparatedQuotedString($paisosArticles);
			$where .= " AND aut.country IN ($inPaisosArticles) ";
		}
		if(! is_null($idRevisors)){
			//aquí caldria modificar el from per afegir la join. 
			//Només cal afegir la join si escollim el que sigui de l'altra taula
			//review_assignments r, edit_assignments ea
			$additionalFrom.= " , review_assignments r ";
			$inRevisors = $this -> arrayToComaSeparatedString($idRevisors);
			$where .= " AND a.article_id = r.submission_id AND r.reviewer_id IN ($inRevisors) ";
			// subquery $query .= " AND article_id IN (SELECT submission_id FROM review_assignments WHERE reviewer_id IN ($idRevisors)) ";
		}
		if(! is_null($dataIniciSubmitted) and $dataIniciSubmitted != ""){
			$where .= " AND a.date_submitted >= DATE(STR_TO_DATE('$dataIniciSubmitted','%d/%m/%Y')) ";
		}
		if(! is_null($dataFiSubmitted) and $dataFiSubmitted != ""){
			$where .= " AND a.date_submitted <= DATE(STR_TO_DATE('$dataFiSubmitted','%d/%m/%Y')) ";
		}
		if(! is_null($dataIniciAssigned) and $dataIniciAssigned != ""){
			$where .= " AND r.date_assigned >= DATE(STR_TO_DATE('$dataIniciAssigned','%d/%m/%Y')) ";
		}
		if(! is_null($dataFiAssigned) and $dataFiAssigned != ""){
			$where .= " AND r.date_assigned <= DATE(STR_TO_DATE('$dataFiAssigned','%d/%m/%Y')) ";
		}
		if(! is_null($dataIniciCompleted) and $dataIniciCompleted != ""){
			$where .= " AND r.date_completed >= DATE(STR_TO_DATE('$dataIniciCompleted','%d/%m/%Y')) ";
		}
		if(! is_null($dataFiCompleted) and $dataFiCompleted != ""){
			$where .= " AND r.date_completed <= DATE(STR_TO_DATE('$dataFiCompleted','%d/%m/%Y')) ";
		}
		
		if(! is_null($keywords)){
			//aqui hem d'afegir al from les taules relacionades amb les keywords i les joins entre aquestes taules
			//(si les poséssim sempre, en el cas de que un article no tingués cap keyword no sortiria a les estadístiques
			
			$additionalFrom .= ", article_search_objects aso, article_search_object_keywords asok, article_search_keyword_list askl ";
			
			$inKeywords = $this -> arrayToComaSeparatedQuotedString($keywords);
			
			$where .= " AND a.article_id = aso.article_id AND aso.object_id = asok.object_id AND asok.keyword_id = askl.keyword_id AND askl.keyword_text IN (".$inKeywords.") ";
		}
		
		$this->fromWhere = " FROM articles a, users u, authors aut $additionalFrom WHERE a.journal_id = $journalId AND a.user_id = u.user_id AND a.article_id = aut.submission_id $additionalWhere ".$where;
		
		return null;
	}
	
	function contArticlesDUnEstat($status,$idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords){
		//$status = 0 = rebutjats
		//$status = 3 = publicats

		$this->setFromWhere($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$status,$textKeywords);
		$query = "SELECT count(distinct(a.article_id)) as total ".$this->fromWhere;
		
		$result =& $this->retrieve($query);
		$returner = null;
		if ($result->RecordCount() != 0) {
			//echo "<br>result > 0<br>";
			//$returner =& $this->_rowsToArray($result);
			$row = $result->GetRowAssoc(false);
			return $row['total'];
		}else{
			return null;
		}
	}
	
	function getAgrupatPerPaisArticle($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords,$status){
		//retorna els articles amb els atributs corresponents als parametres agrupats segons el país de l'article
		$this->setFromWhere($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$status,$textKeywords);
		$query = "SELECT count(a.article_id) as total, aut.country as pais ".$this->fromWhere. " GROUP BY aut.country ";
		
		$result =& $this->retrieve($query);
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArrayAssoc($result);
		}
		$result->Close();
		
		//intenem arreglar el returner perque a la bbdd es guarda el codi del pais, no el nom
		foreach ($this->countries as $key => $nomPais){
			for($i=0; $i<count($returner); $i++){
				if($returner[$i]['pais'] == $key){
					$returner[$i]['pais'] = $nomPais;
				}
			}
		}
		for($i=0; $i<count($returner); $i++){
			if($returner[$i]['pais'] == ""){
				$returner[$i]['pais'] = "N/A";
			}
		}
		
		return $returner;
	}
	
	function getPublicatsAgrupatPerPaisArticle($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords){
		//retorna els articles PUBLICATS (a la bbdd status = 3) amb els atributs corresponents als parametres agrupats segons el país de l'article
		$status = 3;
		$returner = $this->getAgrupatPerPaisArticle($idAutors, $idEditors, $paisosAutors, $paisosArticles, $idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted, $journalId, $textKeywords, $status);
		return $returner;
	}
	function getRebutjatsAgrupatPerPaisArticle($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords){
		//retorna els articles REBUTJATS (a la bbdd status = 4) amb els atributs corresponents als parametres agrupats segons el país de l'article
		$status = 0;
		$returner = $this->getAgrupatPerPaisArticle($idAutors, $idEditors, $paisosAutors, $paisosArticles, $idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted, $journalId, $textKeywords, $status);
		return $returner;
	}
	
	function getAgrupatPerPaisAutor($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords,$status){
		$this->setFromWhere($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$status,$textKeywords);
		$query = "SELECT count(a.article_id) as total, u.country as pais ".$this->fromWhere. " GROUP BY u.country ";
		
		$result =& $this->retrieve($query);
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArrayAssoc($result);
		}
		$result->Close();
		
		//intenem arreglar el returner perque a la bbdd es guarda el codi del pais, no el nom
		foreach ($this->countries as $key => $nomPais){
			for($i=0; $i<count($returner); $i++){
				if($returner[$i]['pais'] == $key){
					$returner[$i]['pais'] = $nomPais;
				}
			}
		}
		for($i=0; $i<count($returner); $i++){
			if($returner[$i]['pais'] == ""){
				$returner[$i]['pais'] = "N/A";
			}
		}
		
		return $returner;
	}
	
	function getPublicatsAgrupatPerPaisAutor($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords){
		$status = 3;
		$returner = $this->getAgrupatPerPaisAutor($idAutors, $idEditors, $paisosAutors, $paisosArticles, $idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted, $journalId, $textKeywords, $status);
		return $returner;
	}
	function getRebutjatsAgrupatPerPaisAutor($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords){
		$status = 0;
		$returner = $this->getAgrupatPerPaisAutor($idAutors, $idEditors, $paisosAutors, $paisosArticles, $idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted, $journalId, $textKeywords, $status);
		return $returner;
	}
	
	function getAgrupatPerNumero($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords,$status){
		$additionalFrom = " , issue_settings iss,published_articles pa ";
		$additionalWhere = " AND iss.issue_id = pa.issue_id AND iss.setting_name = 'title' AND pa.article_id = a.article_id ";
		
		$this->setFromWhere($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$status,$textKeywords,$additionalFrom,$additionalWhere);
		$query = " SELECT count(a.article_id) as total, iss.setting_value as titol ".$this->fromWhere. " GROUP BY iss.issue_id, iss.setting_value ";
		
		$result =& $this->retrieve($query);
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArrayAssoc($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function getPublicatsAgrupatPerNumero($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords){
		$status = 3;
		$returner = $this->getAgrupatPerNumero($idAutors, $idEditors, $paisosAutors, $paisosArticles, $idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted, $journalId, $textKeywords, $status);
		return $returner;
	}
	
	function getDiesEntreEnviamentIRebudaRevisors($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords){
		
		$additionalFrom = "";
		$additionalWhere = " AND r.date_notified IS NOT NULL AND r.date_confirmed IS NOT NULL ";
		$status = null;
		$this->setFromWhere($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$status,$textKeywords,$additionalFrom,$additionalWhere);
		
		$query = " SELECT datediff(r.date_confirmed,r.date_notified) as dies, count(r.review_id) as total ".$this->fromWhere." GROUP BY dies ";
		
		$result =& $this->retrieve($query);
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArrayAssoc($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	function getDiesEntreRebudaICompletedRevisors($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$textKeywords){
		//  enviament - (review_assignments.date_completed)
		$additionalFrom = "";
		$additionalWhere = " AND r.date_completed IS NOT NULL AND r.date_confirmed IS NOT NULL ";
		$status = null;
		$this->setFromWhere($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$journalId,$status,$textKeywords,$additionalFrom,$additionalWhere);
		$query = " SELECT datediff(r.date_completed,r.date_confirmed) as dies, count(r.review_id) as total ".$this->fromWhere." GROUP BY dies ";
		
		$result =& $this->retrieve($query);
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_rowsToArrayAssoc($result);
		}
        
		$result->Close();
		return $returner;
	}
	
	private function arrayToComaSeparatedString($array){
		$string = $array[0];
		for ($i=1;$i<count($array);$i++){
			$string .= ",".$array[$i];
		}
		
		return $string;
	}
	
	private function arrayToComaSeparatedQuotedString($array){
		$string = "'".$array[0]."'";
		for ($i=1;$i<count($array);$i++){
			$string .= ",'".$array[$i]."'";
		}
		
		return $string;
	}
	
	private function &_rowsToArray($result) {
		$i = 0;
		$arrayResultant = array();
		
		//aixi consultem els noms del result/recorset que ens acaben de passar
		//ho fem per certs problemes de php accedint a recordsets diferents tractant-los com un array
		for($i=0;$i<($result->FieldCount());$i++){
			$nomColumna=$result->FetchField($i);
			$arrayNoms[$i] = $nomColumna->name;
		}
		
		$i = 0;
		while (!$result->EOF) {
			
			$row = $result->GetRowAssoc(false);
			
			for($e = 0;$e<count($arrayNoms);$e++){
				$arrayResultant[$i][$e] = $row[$arrayNoms[$e]];
			}
			$result->MoveNext();
			$i++;
		}
		return $arrayResultant;
	}
	
	private function &_rowsToArrayAssoc($result) {
		$i = 0;
		$arrayResultant = array();
		
		//aixi consultem els noms del result/recorset que ens acaben de passar
		//ho fem per certs problemes de php accedint a recordsets diferents tractant-los com un array
		for($i=0;$i<($result->FieldCount());$i++){
			$nomColumna=$result->FetchField($i);
			$arrayNoms[$i] = $nomColumna->name;
		}
		
		$i = 0;
		while (!$result->EOF) {
			
			$row = $result->GetRowAssoc(false);
			
			for($e = 0;$e<count($arrayNoms);$e++){
				$arrayResultant[$i][$arrayNoms[$e]] = $row[$arrayNoms[$e]];
			}
			$result->MoveNext();
			$i++;
		}
		
		return $arrayResultant;
	}
}

?>