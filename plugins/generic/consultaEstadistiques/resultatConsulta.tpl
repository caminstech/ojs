{**
 * resultatConsulta.tpl
 *
 * 
 *
 * $Id$
 *}
{strip}
{assign var="pageTitle" value="plugins.generic.consultaEstadistiques"}
{include file="common/header.tpl"}
{/strip}
<head>

<link rel="stylesheet" href="{$baseUrl}/plugins/generic/consultaEstadistiques/jquery/jquery-ui-1.10.3/themes/smoothness/jquery-ui.css">
<script src="{$baseUrl}/plugins/generic/consultaEstadistiques/jquery/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
<script type="text/javascript" src="{$baseUrl}/plugins/generic/consultaEstadistiques/js/highcharts/highcharts.js"></script>
<script type="text/javascript" src="{$baseUrl}/plugins/generic/consultaEstadistiques/js/highcharts/modules/exporting.js"></script>

</head>
<div id="div1">
	{if isset($publicats)}
		El total de publicats és de: {$publicats}<br>
	{/if}
	{if isset($rebutjats)}
		El total de rebutjats és de: {$rebutjats}<br>
	{/if}
	{if isset($percentatge)}
		El percentatge de publicació és de: {$percentatge}%<br>
	{/if}
	{if isset($totalDeQualsevolEstat)}
		El total d'articles inpendentment del seu estat és de: {$totalDeQualsevolEstat}<br>
	{/if}
	{if isset($mitjanaDiesEntreEnviamentIRebuda)}
		Mitja dies entre enviament i rebuda: {$mitjanaDiesEntreEnviamentIRebuda} dies<br>
	{/if}
	{if isset($mitjanaDiesEntreRebudaICompleted)}
		Mitja dies entre rebuda i finalització: {$mitjanaDiesEntreRebudaICompleted} dies<br>
	{/if}
	{if $hiHaArticles}
		</div>
		<br><br>
		<div id="tabs">
			<ul>
				{if $publicats > 0}
					<li><a href="#tabs-1">Publicats segon els pais de l'autor</a></li>
					<li><a href="#tabs-2">Publicats segons el pais de l'article</a></li>
					<li><a href="#tabs-3">Publicats per número</a></li>
				{/if}
				{if $rebutjats > 0}
					<li><a href="#tabs-4">Rebutjats segon els pais de l'autor</a></li>
					<li><a href="#tabs-5">Rebutjats segons el pais de l'article</a></li>
				{/if}
				{if $mostrarGraficaRevisors}
					<li><a href="#tabs-6">Dies que triguen a acceptar la revisió</a></li>
					<li><a href="#tabs-7">Dies que triguen a revisar l'article</a></li>
				{/if}
			</ul>

				<div id="tabs-1" {if $publicats == 0} style="display:none" {/if}>
					<div id="containPaisAutor" style="width:500px; height:400px;"></div>
				</div>
				<div id="tabs-2" {if $publicats == 0} style="display:none" {/if}>
					<div id="containPaisArticle" style="width:500px; height:400px;"></div>
				</div>
				<div id="tabs-3" {if $publicats == 0} style="display:none" {/if}>
					<div id="containNumero" style="width:500px; height:400px;"></div>
				</div>

			
			<div id="tabs-4" {if $rebutjats == 0} style="display:none" {/if}>
				<div id="containRebutjatsPaisAutor" style="width:500px; height:400px;"></div>
			</div>
			<div id="tabs-5" {if $rebutjats == 0} style="display:none" {/if}>
				<div id="containRebutjatsPaisArticle" style="width:500px; height:400px;"></div>
			</div>
			

				<div id="tabs-6" {if !$mostrarGraficaRevisors} style="display:none" {/if}>
					<div id="containDiesEntreEnviamentIRebudaRevisors" style="width:500px; height:400px;"></div>
				</div>
				<div id="tabs-7" {if !$mostrarGraficaRevisors} style="display:none" {/if}>
					<div id="containDiesEntreRebudaICompletedRevisors" style="width:500px; height:400px;"></div>
				</div>

		</div>
		
		<form method="post" action="{url page=ConsultaEstadistiques op=descarregarCSV}" id="formDescarregarCSV">
			<input type="hidden" name="publicats" value="{$publicats}">
			<input type="hidden" name="rebutjats" value="{$rebutjats}">
			<input type="hidden" name="percentatge" value="{$percentatge}">
			<input type="hidden" name="publicatsAgrupatsPaisAutor" value="{$publicatsAgrupatsPaisAutor}">
			<input type="hidden" name="publicatsAgrupatsPaisArticle" value="{$publicatsAgrupatsPaisArticle}">
			<input type="hidden" name="publicatsAgrupatsNumero" value="{$publicatsAgrupatsNumero}">
			<input type="hidden" name="rebutjatsAgrupatsPaisAutor" value="{$rebutjatsAgrupatsPaisAutor}">
			<input type="hidden" name="rebutjatsAgrupatsPaisArticle" value="{$rebutjatsAgrupatsPaisArticle}">
			<input type="hidden" name="diesEntreEnviamentIRebudaRevisors" value="{$diesEntreEnviamentIRebudaRevisors}">
			<input type="hidden" name="diesEntreRebudaICompletedRevisors" value="{$diesEntreRebudaICompletedRevisors}">
			<input type="hidden" name="mitjanaDiesEntreEnviamentIRebuda" value="{$mitjanaDiesEntreEnviamentIRebuda}">
			<input type="hidden" name="mitjanaDiesEntreRebudaICompleted" value="{$mitjanaDiesEntreRebudaICompleted}">
			<input type="hidden" name="mostrarGraficaRevisors" value="{$mostrarGraficaRevisors}">
			<input type="hidden" name="hiHaArticles" value="{$hiHaArticles}">
			<input type="hidden" name="idAutors" value="{$idAutors}">
			<input type="submit" name="submitCSV" value="Descarregar les dades en format CSV">
		</form>
		
		<script language="javascript">
		{literal}
		$(function () {
			$('#containPaisAutor').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        title: {
		            text: "Publicats segons el pais de l'autor"
		        },
		        tooltip: {
		    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.y:.0f} '
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Percentatge sobre el total: ',
		            data: [
		                {/literal}
		                	{foreach item=paisAutor from=$publicatsAgrupatsPaisAutor}
								['{$paisAutor.pais}', {$paisAutor.total}],
							{/foreach}
						{literal}
					]
				}]
			});
			Highcharts.setOptions({
			     colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263','#6AF9C4']
			    });
			$('#containPaisArticle').highcharts({
		
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        title: {
		            text: "Publicats segons el pais de l'article"
		        },
		        tooltip: {
		    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.y:.0f} '
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Percentatge sobre el total: ',
		            data: [
		                {/literal}
		                	{foreach item=paisArticle from=$publicatsAgrupatsPaisArticle}
								['{$paisArticle.pais}', {$paisArticle.total}],
							{/foreach}
						{literal}
					]
				}]
			});
			Highcharts.setOptions({
			     colors: ['#FF0000', '#7AFF7A', '#FFCC00', '#CC00FF', '#FF4D00', '#FF9655', '#00CC00','#C2C2C2']
			    });
			$('#containNumero').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        title: {
		            text: "Publicats segons el número de la revista"
		        },
		        tooltip: {
		    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.y:.0f} '
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Percentatge sobre el total: ',
		            data: [
		                {/literal}
		                	{foreach item=numero from=$publicatsAgrupatsNumero}
								['{$numero.titol}', {$numero.total}],
							{/foreach}
						{literal}
					]
				}]
			});
			Highcharts.setOptions({
			     colors: ['#00FFFF', '#740074', '#0022FF', '#22FF00', '#00A1FF', '#009655', '#FFCCFF','#3D3D3D']
			    });
			$('#containRebutjatsPaisAutor').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        title: {
		            text: "Rebutjats segons el pais de l'autor"
		        },
		        tooltip: {
		    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.y:.0f} '
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Percentatge sobre el total: ',
		            data: [
		                {/literal}
		                	{foreach item=paisAutor from=$rebutjatsAgrupatsPaisAutor}
								['{$paisAutor.pais}', {$paisAutor.total}],
							{/foreach}
						{literal}
					]
				}]
			});
			Highcharts.setOptions({
			     colors: ['#223EF5', '#FF6F44', '#1134A0', '#33EE99', '#110022', '#009655', '#FFCCFF','#00A1FF']
			    });
			$('#containRebutjatsPaisArticle').highcharts({
		
		       chart: {
		           plotBackgroundColor: null,
		           plotBorderWidth: null,
		           plotShadow: false
		       },
		       title: {
		           text: "Rebutjats segons el pais de l'article"
		       },
		       tooltip: {
		   	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		       },
		       plotOptions: {
		           pie: {
		               allowPointSelect: true,
		               cursor: 'pointer',
		               dataLabels: {
		                   enabled: true,
		                   color: '#000000',
		                   connectorColor: '#000000',
		                   format: '<b>{point.name}</b>: {point.y:.0f} '
		               }
		           }
		       },
		       series: [{
		           type: 'pie',
		           name: 'Percentatge sobre el total: ',
		           data: [
		               {/literal}
		               	{foreach item=paisArticle from=$rebutjatsAgrupatsPaisArticle}
								['{$paisArticle.pais}', {$paisArticle.total}],
							{/foreach}
						{literal}
					]
				}]
			});
		
			{/literal}
			{if $mostrarGraficaRevisors}
			{literal}
			Highcharts.setOptions({
			     colors: ['#223EF5', '#FF6F44', '#1134A0', '#33EE99', '#110022', '#009655', '#FFCCFF','#00A1FF']
			    });
			$('#containDiesEntreEnviamentIRebudaRevisors').highcharts({
		
			       chart: {
			           plotBackgroundColor: null,
			           plotBorderWidth: null,
			           plotShadow: false
			       },
			       title: {
			           text: "Dies que triguen a acceptar la revisió"
			       },
			       tooltip: {
			   	    pointFormat: 'el {point.percentage:.0f}%</b> dels articles'
			       },
			       plotOptions: {
			           pie: {
			               allowPointSelect: true,
			               cursor: 'pointer',
			               dataLabels: {
			                   enabled: true,
			                   color: '#000000',
			                   connectorColor: '#000000',
			                   format: '<b>{point.name}</b>: {point.y:.0f} '
			               }
			           }
			       },
			       series: [{
			           type: 'pie',
			           name: 'Percentatge sobre el total: ',
			           data: [
			               {/literal}
			               	{foreach item=enviament from=$diesEntreEnviamentIRebudaRevisors}
									['{$enviament.dies}', {$enviament.total}],
								{/foreach}
							{literal}
						]
					}]
				});
			
			Highcharts.setOptions({
				colors: ['#223EF5', '#FF6F44', '#1134A0', '#33EE99', '#110022', '#009655', '#FFCCFF','#00A1FF']
			    });
			
			$('#containDiesEntreRebudaICompletedRevisors').highcharts({
		
		       chart: {
		           plotBackgroundColor: null,
		           plotBorderWidth: null,
		           plotShadow: false
		       },
		       title: {
		           text: "Dies que triguen a revisar l'article"
		       },
		       tooltip: {
		   	    pointFormat: 'el {point.percentage:.0f}%</b> dels articles'
		       },
		       plotOptions: {
		           pie: {
		               allowPointSelect: true,
		               cursor: 'pointer',
		               dataLabels: {
		                   enabled: true,
		                   color: '#000000',
		                   connectorColor: '#000000',
		                   format: '<b>{point.name}</b>: {point.y:.0f} '
		               }
		           }
		       },
		       series: [{
		           type: 'pie',
		           name: 'Percentatge sobre el total: ',
		           data: [
		               {/literal}
		               	{foreach item=enviament from=$diesEntreRebudaICompletedRevisors}
								['{$enviament.dies}', {$enviament.total}],
							{/foreach}
						{literal}
					]
				}]
			});
			{/literal}
			{/if}
			{literal}
		});
		$(document).ready(function () {
		
		    $("#tabs").tabs();
		
		});
		
		{/literal}
		</script>
	{else}
		<br><strong>Els paràmetres introduits no concorden amb cap article publicat o rebutjat</strong>
	{/if}