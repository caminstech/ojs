<?php
import('classes.handler.Handler');

class ConsultaEstadistiquesHandler extends Handler{
	var $plugin;
	var $consultaEstadistiquesDAO;
	var $journal_id;
	var $countries;
	
	function ConsultaEstadistiquesHandler() {
		parent::Handler();
		$this->validate();
		$plugin = & $this->plugin;
		$journal =& Request::getJournal();
		$this->journal_id = $journal->_data['id'];
		
		$plugin->import('ConsultaEstadistiquesDAO');
		$this->consultaEstadistiquesDAO = new ConsultaEstadistiquesDAO();
		DAORegistry::registerDAO('ConsultaEstadistiquesDAO', $this->consultaEstadistiquesDAO);
		
		$countryDao =& DAORegistry::getDAO('CountryDAO');
		$this->countries =& $countryDao->getCountries();
	}
	
	/*
	 * Assigna a smarty variables necessàries per fer funcionar el plugin sense javascript 
	 * (si hi ha javascript després el template es comporta d'una forma diferent.
	 */
	function index(){
		$this->validate();
		$plugin = & $this->plugin;
		
		//Li passem les dades que ha de mostrar i mostrem el template
		$templateManager = $this->inicialitzaTemplateManager();
		$templateManager->display($plugin->getTemplatePath() . 'formulariConsultaEstadistiques.tpl');
		
		return false;
	}
	
	/*
	 * obtenim les dades dels articles que coincideixen amb els paràmetres especificats anteriorment
	 */
	function resultatConsulta(){
		//aqui les crides al dao que construiran la query
		$this->validate();
		$plugin = & $this->plugin;
		$plugin->import('ConsultaEstadistiquesDAO');
		$consultaEstadistiquesDAO = new ConsultaEstadistiquesDAO();
		DAORegistry::registerDAO('ConsultaEstadistiquesDAO', $consultaEstadistiquesDAO);
		
		//aquests són els valor introduits per l'usuari
		$idAutors = $_POST['autors'];
		$idEditors = $_POST['editors'];
		$idRevisors = $_POST['revisors'];
		$paisosAutors = $_POST['paisAutors'];
		$paisosArticles = $_POST['paiosArticles'];
		$textKeywords = $_POST['keywords'];
		$dataIniciSubmitted = $_POST['dataIniciSubmitted'];
		$dataFiSubmitted = $_POST['dataFiSubmitted'];
		$dataIniciAssigned = $_POST['dataIniciAssigned'];
		$dataFiAssigned = $_POST['dataFiAssigned'];
		$dataIniciCompleted = $_POST['dataIniciCompleted'];
		$dataFiCompleted = $_POST['dataFiCompleted'];
		$journal =& Request::getJournal();
		$this->journal_id = $journal->_data['id'];
		
		//amb les variables donades, aconsegueixo les quantitats d'articles classificats enre publicats i rebutjats
		$publicats = $consultaEstadistiquesDAO->contPublicats($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted, $this->journal_id,$textKeywords);
		$rebutjats = $consultaEstadistiquesDAO->contRebutjats($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted, $this->journal_id,$textKeywords);
		$totalDeQualsevolEstat = $consultaEstadistiquesDAO->contArticlesDeQualsevolEstat($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted, $this->journal_id,$textKeywords);
		$percentatge = round(($publicats/($publicats+$rebutjats))*100);
		
		$hiHaArticles = false;
		if($publicats > 0 or $rebutjats > 0){
			//si hi ha articles que coincideixen amb la cerca, els agrupem de diferents formes per mostrarlos classificats
			$hiHaArticles = true;
			
			$publicatsAgrupatsPaisArticle = $consultaEstadistiquesDAO->getPublicatsAgrupatPerPaisArticle($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$this->journal_id,$textKeywords);
			$publicatsAgrupatsPaisAutor = $consultaEstadistiquesDAO->getPublicatsAgrupatPerPaisAutor($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$this->journal_id,$textKeywords);
			$publicatsAgrupatsNumero = $consultaEstadistiquesDAO->getPublicatsAgrupatPerNumero($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$this->journal_id,$textKeywords);
			$rebutjatsAgrupatsPaisArticle = $consultaEstadistiquesDAO->getRebutjatsAgrupatPerPaisArticle($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$this->journal_id,$textKeywords);
			$rebutjatsAgrupatsPaisAutor = $consultaEstadistiquesDAO->getRebutjatsAgrupatPerPaisAutor($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted,$this->journal_id,$textKeywords);
			
			//les gràfiques dels revisors només cal mostrar-les si s'ha filtrat per algun revisor.
			//En cas contrari estarien buides i no les mostrem
			$mostrarGraficaRevisors = false;
			if($idRevisors != null){
				$mostrarGraficaRevisors = true;
				
				$diesEntreEnviamentIRebudaRevisors = $consultaEstadistiquesDAO->getDiesEntreEnviamentIRebudaRevisors($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted, $this->journal_id,$textKeywords);
				$diesEntreRebudaICompletedRevisors = $consultaEstadistiquesDAO->getDiesEntreRebudaICompletedRevisors($idAutors,$idEditors,$paisosAutors,$paisosArticles,$idRevisors,$dataIniciSubmitted,$dataFiSubmitted,$dataIniciAssigned,$dataFiAssigned,$dataIniciCompleted,$dataFiCompleted, $this->journal_id,$textKeywords);
				
				//calculo la mitja dels dies entre la petició de la revisió i la confirmació per part del revisor
				$mitjanaDiesEntreEnviamentIRebuda = 0;
				$totalDies = 0;
				$totalArticles = 0;
				for($i = 0;$i<count($diesEntreEnviamentIRebudaRevisors);$i++){
					$totalDies += $diesEntreEnviamentIRebudaRevisors[$i]['dies'];
					$totalArticles += $diesEntreEnviamentIRebudaRevisors[$i]['total'];
					$mitjanaDiesEntreEnviamentIRebuda += $diesEntreEnviamentIRebudaRevisors[$i]['dies']*$diesEntreEnviamentIRebudaRevisors[$i]['total'];
				}
				$mitjanaDiesEntreEnviamentIRebuda != 0 ? $mitjanaDiesEntreEnviamentIRebuda /= $totalArticles : $mitjanaDiesEntreEnviamentIRebuda = 0;
				
				$totalDies = 0;
				$totalArticles = 0;
				$mitjanaDiesEntreRebudaICompleted = 0;
				//ara la mitja dels dies entre que ha confirmat que farà la revisió, fins que la completa
				for($i = 0;$i<count($diesEntreRebudaICompletedRevisors);$i++){
					$totalDies += $diesEntreRebudaICompletedRevisors[$i]['dies'];
					$totalArticles += $diesEntreRebudaICompletedRevisors[$i]['total'];
					$mitjanaDiesEntreRebudaICompleted += $diesEntreRebudaICompletedRevisors[$i]['dies']*$diesEntreRebudaICompletedRevisors[$i]['total'];
				}
				$mitjanaDiesEntreRebudaICompleted != 0 ? $mitjanaDiesEntreRebudaICompleted /= $totalArticles : $mitjanaDiesEntreRebudaICompleted = 0;
				
				//agrupem els dies que han trigat a fer les revisions en els seguents grups:
				$diesEntreEnviamentIRebudaRevisorsAgrupats = array();
				//agruparé les revisions per: respostes el mateix dia, entre 1 i 2, entre 2 i 7, entre 7 i 14 entre 14 i 30 i més de 30.
				$diesEntreEnviamentIRebudaRevisorsAgrupats[0]['dies'] = "el mateix dia";
				$diesEntreEnviamentIRebudaRevisorsAgrupats[1]['dies'] = "abans de 2 dies";
				$diesEntreEnviamentIRebudaRevisorsAgrupats[2]['dies'] = "la mateixa setmana";
				$diesEntreEnviamentIRebudaRevisorsAgrupats[3]['dies'] = "abans de 2 setmanes";
				$diesEntreEnviamentIRebudaRevisorsAgrupats[4]['dies'] = "el mateix mes";
				$diesEntreEnviamentIRebudaRevisorsAgrupats[5]['dies'] = "més tard d un mes";
				
				for($i=0; $i<count($diesEntreEnviamentIRebudaRevisorsAgrupats); $i++){
					$diesEntreEnviamentIRebudaRevisorsAgrupats[$i]['total']=0;
				}
				
				for($i = 0; $i<count($diesEntreEnviamentIRebudaRevisors); $i++){
					switch (true){
						case $diesEntreEnviamentIRebudaRevisors[$i]['dies'] == 0:
							$diesEntreEnviamentIRebudaRevisorsAgrupats[0]['total'] += $diesEntreEnviamentIRebudaRevisors[$i]['total'];
						break;
						case $diesEntreEnviamentIRebudaRevisors[$i]['dies'] > 0 and $diesEntreEnviamentIRebudaRevisors[$i]['dies'] < 2:
							$diesEntreEnviamentIRebudaRevisorsAgrupats[1]['total'] += $diesEntreEnviamentIRebudaRevisors[$i]['total'];
						break;
						case $diesEntreEnviamentIRebudaRevisors[$i]['dies'] >= 2 and $diesEntreEnviamentIRebudaRevisors[$i]['dies'] < 7:
							$diesEntreEnviamentIRebudaRevisorsAgrupats[2]['total'] += $diesEntreEnviamentIRebudaRevisors[$i]['total'];
						break;
						case $diesEntreEnviamentIRebudaRevisors[$i]['dies'] >= 7 and $diesEntreEnviamentIRebudaRevisors[$i]['dies'] < 14:
							$diesEntreEnviamentIRebudaRevisorsAgrupats[3]['total'] += $diesEntreEnviamentIRebudaRevisors[$i]['total'];
						break;
						case $diesEntreEnviamentIRebudaRevisors[$i]['dies'] >= 14 and $diesEntreEnviamentIRebudaRevisors[$i]['dies'] < 30:
							$diesEntreEnviamentIRebudaRevisorsAgrupats[4]['total'] += $diesEntreEnviamentIRebudaRevisors[$i]['total'];
						break;
						case $diesEntreEnviamentIRebudaRevisors[$i]['dies'] >= 30:
							$diesEntreEnviamentIRebudaRevisorsAgrupats[5]['total'] += $diesEntreEnviamentIRebudaRevisors[$i]['total'];
						break;
					}
				}
			}
			
			//agrupem els dies que han trigat a fer les revisions en els seguents grups:
			$diesEntreRebudaICompletedRevisorsAgrupats = array();
			//agruparé les revisions per: finalitzades abans d'1 mes, entre 1 i 2 mesos, entre 2 i 3, i més de 3 
			$diesEntreRebudaICompletedRevisorsAgrupats[0]['dies'] = "menys dun mes";
			$diesEntreRebudaICompletedRevisorsAgrupats[1]['dies'] = "entre 1 o 2 mesos";
			$diesEntreRebudaICompletedRevisorsAgrupats[2]['dies'] = "entre 2 i 3 mesos";
			$diesEntreRebudaICompletedRevisorsAgrupats[3]['dies'] = "més de 3 mesos";
			
			for($i=0; $i<count($diesEntreRebudaICompletedRevisorsAgrupats); $i++){
				$diesEntreRebudaICompletedRevisorsAgrupats[$i]['total']=0;
			}
			
			for($i = 0; $i<count($diesEntreRebudaICompletedRevisors); $i++){
				switch (true){
					case $diesEntreRefalsebudaICompletedRevisors[$i]['dies'] < 30:
						$diesEntreRebudaICompletedRevisorsAgrupats[0]['total'] += $diesEntreRebudaICompletedRevisors[$i]['total'];
						break;
					case $diesEntreRebudaICompletedRevisors[$i]['dies'] >= 30 and $diesEntreRebudaICompletedRevisors[$i]['dies'] < 60:
						$diesEntreRebudaICompletedRevisorsAgrupats[1]['total'] += $diesEntreRebudaICompletedRevisors[$i]['total'];
						break;
					case $diesEntreRebudaICompletedRevisors[$i]['dies'] >= 60 and $diesEntreRebudaICompletedRevisors[$i]['dies'] <= 90:
						$diesEntreRebudaICompletedRevisorsAgrupats[2]['total'] += $diesEntreRebudaICompletedRevisors[$i]['total'];
						break;
					case $diesEntreRebudaICompletedRevisors[$i]['dies'] > 90:
						$diesEntreRebudaICompletedRevisorsAgrupats[3]['total'] += $diesEntreRebudaICompletedRevisors[$i]['total'];
						break;
				}
			}
		}
		
		//assignem els valors a smarty i mostrem el template
		$templateManager = $this->inicialitzaTemplateManager();
		$templateManager->assign('publicats', $publicats);
		$templateManager->assign('rebutjats', $rebutjats);
		$templateManager->assign('totalDeQualsevolEstat', $totalDeQualsevolEstat);
		$templateManager->assign('percentatge', $percentatge);
		$templateManager->assign('publicatsAgrupatsPaisAutor', $publicatsAgrupatsPaisAutor);
		$templateManager->assign('publicatsAgrupatsPaisArticle', $publicatsAgrupatsPaisArticle);
		$templateManager->assign('publicatsAgrupatsNumero', $publicatsAgrupatsNumero);
		$templateManager->assign('rebutjatsAgrupatsPaisAutor', $rebutjatsAgrupatsPaisAutor);
		$templateManager->assign('rebutjatsAgrupatsPaisArticle', $rebutjatsAgrupatsPaisArticle);
		$templateManager->assign('diesEntreEnviamentIRebudaRevisors', $diesEntreEnviamentIRebudaRevisorsAgrupats);
		$templateManager->assign('diesEntreRebudaICompletedRevisors', $diesEntreRebudaICompletedRevisorsAgrupats);
		$templateManager->assign('mitjanaDiesEntreEnviamentIRebuda', $mitjanaDiesEntreEnviamentIRebuda);
		$templateManager->assign('mitjanaDiesEntreRebudaICompleted', $mitjanaDiesEntreRebudaICompleted);
		$templateManager->assign('mostrarGraficaRevisors', $mostrarGraficaRevisors);
		$templateManager->assign('hiHaArticles', $hiHaArticles);
		$templateManager->assign('idAutors', implode(",",$_POST['autors']));
		

		$templateManager->display($plugin->getTemplatePath() . 'resultatConsulta.tpl');
	}
	
	function descarregarCSV(){
		$this->validate();
		$plugin = & $this->plugin;
		
		$publicats = $_POST['publicats'];
		$rebutjats = $_POST['rebutjats'];
		$percentatge = $_POST['percentatge'];
		$publicatsAgrupatsPaisAutor = $_POST['publicatsAgrupatsPaisAutor'];
		$publicatsAgrupatsPaisArticle = $_POST['publicatsAgrupatsPaisArticle'];
		$publicatsAgrupatsNumero = $_POST['publicatsAgrupatsNumero'];
		$rebutjatsAgrupatsPaisAutor = $_POST['rebutjatsAgrupatsPaisAutor'];
		$rebutjatsAgrupatsPaisArticle = $_POST['rebutjatsAgrupatsPaisArticle'];
		$diesEntreEnviamentIRebudaRevisorsAgrupats = $_POST['diesEntreEnviamentIRebudaRevisors'];
		$diesEntreRebudaICompletedRevisorsAgrupats = $_POST['diesEntreRebudaICompletedRevisors'];
		$mitjanaDiesEntreEnviamentIRebuda = $_POST['mitjanaDiesEntreEnviamentIRebuda'];
		$mitjanaDiesEntreRebudaICompleted = $_POST['mitjanaDiesEntreRebudaICompleted'];
		$mostrarGraficaRevisors = $_POST['mostrarGraficaRevisors'];
		$hiHaArticles = $_POST['hiHaArticles'];
		$idAutors = explode(",",$_POST['idAutors']);
		
		
		
		
		
		
		//assignem els valors a smarty i mostrem el template de descarrega de les dades en csv
		$templateManager = $this->inicialitzaTemplateManager();
		$templateManager->assign('publicats', $publicats);
		$templateManager->assign('rebutjats', $rebutjats);
		$templateManager->assign('percentatge', $percentatge);
		$templateManager->assign('publicatsAgrupatsPaisAutor', $publicatsAgrupatsPaisAutor);
		$templateManager->assign('publicatsAgrupatsPaisArticle', $publicatsAgrupatsPaisArticle);
		$templateManager->assign('publicatsAgrupatsNumero', $publicatsAgrupatsNumero);
		$templateManager->assign('rebutjatsAgrupatsPaisAutor', $rebutjatsAgrupatsPaisAutor);
		$templateManager->assign('rebutjatsAgrupatsPaisArticle', $rebutjatsAgrupatsPaisArticle);
		$templateManager->assign('diesEntreEnviamentIRebudaRevisors', $diesEntreEnviamentIRebudaRevisorsAgrupats);
		$templateManager->assign('diesEntreRebudaICompletedRevisors', $diesEntreRebudaICompletedRevisorsAgrupats);
		$templateManager->assign('mitjanaDiesEntreEnviamentIRebuda', $mitjanaDiesEntreEnviamentIRebuda);
		$templateManager->assign('mitjanaDiesEntreRebudaICompleted', $mitjanaDiesEntreRebudaICompleted);
		$templateManager->assign('mostrarGraficaRevisors', $mostrarGraficaRevisors);
		$templateManager->assign('hiHaArticles', $hiHaArticles);
		
		
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$templateManager->display($plugin->getTemplatePath() . 'descarregarCSV.tpl');
	}
	
	//devolvemos el id y el nombre de todos los autores que han enviado alguna vez algún artículo, en formato JSON
	function ajaxGetAutorsJSON(){
		$this->validate();
		$plugin = & $this->plugin;
		
		$journal =& Request::getJournal();
		$this->journal_id = $journal->_data['id'];
		
		$arrayAutors = $this->consultaEstadistiquesDAO->getAutorsAssoc($this->journal_id);
		
		echo json_encode($arrayAutors);
	}
	
	function getNomAutors($arrayIdAutors){
		$this->validate();
		$plugin = & $this->plugin;
		
		$arrayNomAutors = $this->consultaEstadistiquesDAO->getNomAutorsByIdAssoc($arrayIdAutors);
		
		return $arrayNomAutors;
	}
	
	//devolvemos el id y el nombre de todos los editores que han editado alguna vez algún artículo, en formato JSON
	function ajaxGetEditorsJSON(){
		$this->validate();
		$plugin = & $this->plugin;
		
		$journal =& Request::getJournal();
		$this->journal_id = $journal->_data['id'];
		
		$arrayEditors = $this->consultaEstadistiquesDAO->getEditorsAssoc($this->journal_id);
		
		echo json_encode($arrayEditors);
	}
	
	//devolvemos el id y el nombre de todos los revisores que han revisado alguna vez algún artículo, en formato JSON
	function ajaxGetRevisorsJSON(){
		$journal =& Request::getJournal();
		$this->journal_id = $journal->_data['id'];
		
		$arrayRevisors = $this->consultaEstadistiquesDAO->getRevisorsAssoc($this->journal_id);
		
		echo json_encode($arrayRevisors);
	}
	
	//devolvemos el id y el nombre de todos países de procedencia de los autores que han enviado alguna vez algún artículo, en formato JSON
	function ajaxGetPaisosAutorsJSON(){
		$journal =& Request::getJournal();
		$this->journal_id = $journal->_data['id'];
		
		$countryDao =& DAORegistry::getDAO('CountryDAO');
		$countries =& $countryDao->getCountries();
		
		$arrayKeyValuePaisosAutors = array();
		//aixi aconseguim els codis dels paisos dels autors
		$arrayPaisosAutors = $this->consultaEstadistiquesDAO->getPaisosAutorsAssoc($this->journal_id);
		//ara aconseguim el nom del pais
		foreach ($countries as $key => $nomPais){
			foreach($arrayPaisosAutors as $paisAutor){
				if($key == $paisAutor['country']){
					$arrayKeyValuePaisosAutors[] = array("key" => $key, "name" => $nomPais);
				}
			}
		}
		
		echo json_encode($arrayKeyValuePaisosAutors);
	}
	
	//devolvemos el id y el nombre de todos los países que se han relacionado alguna vez con algún artículo, en formato JSON
	function ajaxGetPaisosArticlesJSON(){
		$journal =& Request::getJournal();
		$this->journal_id = $journal->_data['id'];
				
		$countryDao =& DAORegistry::getDAO('CountryDAO');
		$countries =& $countryDao->getCountries();
		$arrayKeyValuePaisosArticles = array();
		//aixi aconseguim els codis dels paisos dels autors
		$arrayPaisosArticles = $this->consultaEstadistiquesDAO->getPaisosArticlesAssoc($this->journal_id);
		//ara aconseguim el nom del pais
		foreach ($countries as $key => $nomPais){
			foreach($arrayPaisosArticles as $paisArticle){
				if($key == $paisArticle['country']){
					$arrayKeyValuePaisosArticles[] = array("key" => $key, "name" => $nomPais);
				}
			}
		}
		echo json_encode($arrayKeyValuePaisosArticles);
	}
	
	//devolvemos el id y el texto de las keywords que han aparecido alguna vez en algún artículo, en formato JSON
	function ajaxGetKeywordsJSON(){
		$journal =& Request::getJournal();
		$this->journal_id = $journal->_data['id'];

		$arrayKeywords = $this->consultaEstadistiquesDAO->getKeywordsAssoc($this->journal_id);

		echo json_encode($arrayKeywords);
	}
	
	function mostrarTemplate($template,$arrayParams = NULL){

		$this->setupTemplate();
		$plugin =& $this->plugin;
		
		$templateManager =& TemplateManager::getManager();
		$templateManager->assign('path', $plugin->getTemplatePath());
		
		foreach ($arrayParams as $param){
			$templateManager->assign($param);
		}
		$templateManager->display($plugin->getTemplatePath() . $template);
	}
	
	function inicialitzaTemplateManager(){
		$this->setupTemplate();
		$plugin =& $this->plugin;
		
		$templateManager =& TemplateManager::getManager();
		$templateManager->assign('path', $plugin->getTemplatePath());
		
		return $templateManager;
	}
	
	function validate($canRedirect = true) {
		parent::validate();
		$journal =& Request::getJournal();
		
		//cambio para test en entorno de pruebas de revistes.upc
		//if (!Validation::isSiteAdmin() and !Validation::isJournalManager($this->journal_id)) {
		//	if ($canRedirect) Validation::redirectLogin();
		//	else exit;
		//}

		$plugin =& Registry::get('plugin');
		$this->plugin =& $plugin;
		return true;
	}
	
	function setupTemplate($subclass = false) {
		parent::setupTemplate();
		$templateMgr =& TemplateManager::getManager();

		$pageHierarchy = array(array(Request::url(null, 'user'), 'navigation.user'));

		//cambiar counter per consultarEstadistiques
		if ($subclass) $pageHierarchy[] = array(Request::url(null, 'consultaEstadistiques'), 'plugins.generic.consultaEstadistiques');
		
		$templateMgr->assign_by_ref('pageHierarchy', $pageHierarchy);
	}
}
