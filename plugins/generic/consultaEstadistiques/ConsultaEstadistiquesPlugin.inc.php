<?php
import('classes.plugins.GenericPlugin');
class ConsultaEstadistiquesPlugin extends GenericPlugin {
    function register($category, $path) {
        if (parent::register($category, $path)) {
            HookRegistry::register('Templates::Manager::Index::ManagementPages', array(&$this, 'callback'));
            HookRegistry::register ('LoadHandler', array(&$this, 'handleRequest'));
            
            $this->addLocaleData();
            
            return true;
        }
        return false;
    }
    function getName() {
        return 'ConsultaEstadistiquesPlugin';
    }
    function getDisplayName() {
        return 'ConsultaEstadistiques Plugin';
    }
    function getDescription() {
        return 'A description of this plugin';
    }
    
    function callback($hookName, $args) {
		$params =& $args[0];
		$smarty =& $args[1];
		$output =& $args[2];
		$output .= '<li>&#187; <a href="' . Request::url( null, 'ConsultaEstadistiques' ) . '">' . Locale::translate( 'plugins.generic.consultaEstadistiques.linkname' ) . '</a></li>';
		
		return false;
	}
    
    function handleRequest( $hookName, $args ){
    	
		$page =& $args[0];
		$op =& $args[1];
		$sourceFile =& $args[2];
		
		//var_dump($args);
		
		if ( $page === 'ConsultaEstadistiques' ){
			$this->addLocaleData();
			$this->import( 'ConsultaEstadistiquesHandler' );
			Registry::set( 'plugin', $this );
			define( 'HANDLER_CLASS', 'ConsultaEstadistiquesHandler' );
	        
			return true;
		}
		
		return false;
    }
    
	function getManagementVerbs() {
		$verbs = array();
		if ($this->getEnabled()) {
			$verbs[] = array('disable',Locale::translate('manager.plugins.disable'));
			//$verbs[] = array('settings',Locale::translate('manager.plugins.settings'));
		} else {
			$verbs[] = array('enable',Locale::translate('manager.plugins.enable'));
		}
		return $verbs;
	}
}
