<?php

import('classes.plugins.GenericPlugin');

class LinkAGHPlugin 
extends GenericPlugin {
    
	function register($category, $path) {
        if (parent::register($category, $path)) {
        	if($this->getEnabled()){
	        	//Registry::set( 'LinkAGH', $this );
	        	
	            HookRegistry::register ('Templates::Common::Header::Navbar::CurrentJournal', array(&$this, 'displayHeaderLink'));
	         }
	         $this->addLocaleData();
	
	         return true;
        }
        return false;
    }
    function getName() {
        return Locale::translate( 'plugins.generic.linkAGH' );
    }
    function getDisplayName() {
        return Locale::translate( 'plugins.generic.linkAGH' );
    }
    function getDescription() {
    	return Locale::translate( 'plugins.generic.linkAGH.description' );
    }
	
	function displayHeaderLink($hookName, $args) {
		$params =& $args[0];
		$smarty =& $args[1];
		$output =& $args[2];
		$output .= '<li><a href="http://revistes.ub.edu/index.php/ActaGeologica/issue/archive">Acta Geologica Hispanica</a></li>';
		
		return false;
	}
	
	function getManagementVerbs() {
		$verbs = array();
		if ($this->getEnabled()) {
			$verbs[] = array('disable',Locale::translate('manager.plugins.disable'));
		} else {
			$verbs[] = array('enable',Locale::translate('manager.plugins.enable'));
		}
		return $verbs;
	}
}

?>
